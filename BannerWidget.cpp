//------------------------------------------------------------------------------
//
// BannerWidget.cpp created by Yyhrs 2017
//
//------------------------------------------------------------------------------

#include <QPainter>

#include "BannerWidget.hpp"

BannerWidget::BannerWidget(QWidget *parent):
	QWidget(parent), scrollPos(0)
{
	m_staticText.setTextFormat(Qt::PlainText);

	setFixedHeight(fontMetrics().height());
	leftMargin = height() / 3;

	setSeparator("   ---   ");

	connect(&timer, SIGNAL(timeout()), this, SLOT(timer_timeout()));
	timer.setInterval(50);
}

QString BannerWidget::text() const
{
	return m_text;
}

void BannerWidget::setText(const QString &text)
{
	m_text = text;
	updateText();
	update();
}

QString BannerWidget::separator() const
{
	return m_separator;
}

void BannerWidget::setSeparator(const QString &separator)
{
	m_separator = separator;
	updateText();
	update();
}

void BannerWidget::updateText()
{
	timer.stop();

	m_singleTextWidth = fontMetrics().horizontalAdvance(m_text);
	scrollEnabled = (m_singleTextWidth > width() - leftMargin);

	if (scrollEnabled)
	{
		scrollPos = -64;
		m_staticText.setText(m_text + m_separator);
		timer.start();
	}
	else
		m_staticText.setText(m_text);

	m_staticText.prepare(QTransform(), font());
	m_wholeTextSize = QSize(fontMetrics().horizontalAdvance(m_staticText.text()), fontMetrics().height());
}

void BannerWidget::paintEvent(QPaintEvent *)
{
	QPainter p(this);

	if (scrollEnabled)
	{
		buffer.fill(qRgba(0, 0, 0, 0));
		QPainter pb(&buffer);
		pb.setPen(p.pen());
		pb.setFont(p.font());

		int x = qMin(-scrollPos, 0) + leftMargin;
		while (x < width())
		{
			pb.drawStaticText(QPointF(x, (height() - m_wholeTextSize.height()) / 2) + QPoint(2, 2), m_staticText);
			x += m_wholeTextSize.width();
		}

		//Apply Alpha Channel
		pb.setCompositionMode(QPainter::CompositionMode_DestinationIn);
		pb.setClipRect(width() - 15, 0, 15, height());
		pb.drawImage(0, 0, alphaChannel);
		pb.setClipRect(0, 0, 15, height());
		//initial situation: don't apply alpha channel in the left half of the image at all; apply it more and more until scrollPos gets positive
		if (scrollPos < 0)
			pb.setOpacity(static_cast<qreal>(qMax(-8, scrollPos) + 8) / 8.0);
		pb.drawImage(0, 0, alphaChannel);

		//pb.end();
		p.drawImage(0, 0, buffer);
	}
	else
		p.drawStaticText(QPointF(leftMargin, (height() - m_wholeTextSize.height()) / 2), m_staticText);
}

void BannerWidget::resizeEvent(QResizeEvent *)
{
	//When the widget is resized, we need to update the alpha channel.

	alphaChannel = QImage(size(), QImage::Format_ARGB32_Premultiplied);
	buffer = QImage(size(), QImage::Format_ARGB32_Premultiplied);

	//Create Alpha Channel:
	if (width() > 64)
	{
		//create first scanline
		QRgb *scanline1 = (QRgb *)alphaChannel.scanLine(0);
		for (int x = 1; x < 16; ++x)
			scanline1[x - 1] = scanline1[width() - x] = qRgba(0, 0, 0, x << 4);
		for (int x = 15; x < width() - 15; ++x)
			scanline1[x] = qRgb(0, 0, 0);
		//copy scanline to the other ones
		for (int y = 1; y < height(); ++y)
			memcpy(alphaChannel.scanLine(y), (uchar *)scanline1, width() * 4);
	}
	else
		alphaChannel.fill(qRgb(0, 0, 0));

	//Update scrolling state
	bool newScrollEnabled = (m_singleTextWidth > width() - leftMargin);
	if (newScrollEnabled != scrollEnabled)
		updateText();
}

void BannerWidget::timer_timeout()
{
	scrollPos = (scrollPos + 2)
		% m_wholeTextSize.width();
	update();
}
