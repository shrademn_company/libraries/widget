//------------------------------------------------------------------------------
//
// BannerWidget.hpp created by Yyhrs 2017
//
//------------------------------------------------------------------------------

#ifndef BANNERWIDGET_HPP
#define BANNERWIDGET_HPP

#include <QStaticText>
#include <QTimer>
#include <QWidget>

class BannerWidget: public QWidget
{
	Q_OBJECT
	Q_PROPERTY(QString text READ text WRITE setText)
	Q_PROPERTY(QString separator READ separator WRITE setSeparator)

public:
	explicit BannerWidget(QWidget *parent = 0);

public slots:
	QString text() const;
	void	setText(const QString &text);

	QString separator() const;
	void	setSeparator(const QString &separator);

protected:
	virtual void	paintEvent(QPaintEvent *);
	virtual void	resizeEvent(QResizeEvent *);

private:
	void updateText();

	QString		m_text;
	QString		m_separator;
	QStaticText m_staticText;
	int			m_singleTextWidth{};
	QSize		m_wholeTextSize;
	int			leftMargin;
	bool		scrollEnabled{};
	int			scrollPos;
	QImage		alphaChannel;
	QImage		buffer;
	QTimer		timer;

private slots:
	virtual void timer_timeout();
};

#endif // BANNERWIDGET_HPP
