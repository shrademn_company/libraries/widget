//------------------------------------------------------------------------------
//
// ColorButton.cpp created by Yyhrs 2019-07-26
//
//------------------------------------------------------------------------------

#include <QColorDialog>

#include "ColorButton.hpp"

ColorButton::ColorButton(QWidget *parent):
	QPushButton{parent}
{
	setStyleSheet("min-width: 22px;width: 22px;height: 22px;padding: 0px 0px 0px 0px;border: 2px solid black;");
	setIconSize({c_size, c_size});
	connect(this, &QPushButton::clicked, [this]()
	{
		setColor(QColorDialog::getColor(m_color));
	});
}

const QColor &ColorButton::color() const
{
	return m_color;
}

void ColorButton::setColor(const QColor &color)
{
	QPixmap	pix{c_size, c_size};

	m_color = color;
	pix.fill(m_color);
	setIcon(pix);
}
