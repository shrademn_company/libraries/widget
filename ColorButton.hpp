//------------------------------------------------------------------------------
//
// ColorButton.hpp created by Yyhrs 2019-07-26
//
//------------------------------------------------------------------------------

#ifndef COLORBUTTON_HPP
#define COLORBUTTON_HPP

#include <QPushButton>

class ColorButton: public QPushButton
{
	Q_OBJECT

public:
	explicit ColorButton(QWidget *parent = nullptr);
	~ColorButton() override = default;

	const QColor	&color() const;
	void			setColor(const QColor &color);

private:
	Q_DISABLE_COPY(ColorButton)

	QColor m_color;

	static constexpr int c_size{24};
};

#endif // COLORBUTTON_HPP
