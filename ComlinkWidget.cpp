//------------------------------------------------------------------------------
//
// ComlinkWidget.cpp created by Yyhrs 2019-08-14
//
//------------------------------------------------------------------------------

#include <QDebug>
#include <QMenu>
#include <QNetworkDatagram>
#include <QNetworkInterface>

#include "ComlinkWidget.hpp"

ComlinkWidget::ComlinkWidget(QWidget *parent):
	QWidget{parent}
{
	for (const auto &address: QNetworkInterface::allAddresses())
		if (address.protocol() == QUdpSocket::IPv4Protocol && (address.isGlobal() || address.isLinkLocal()))
			m_addresses << address.toString();
	setupUi(this);
	m_timer.start(c_delay);
	nameLineEdit->setText(qAppName());
	connectActions();
	connectWidgets();
	connectSockets();
}

ComlinkWidget::~ComlinkWidget()
{
	m_comlinkSocket.close();
}

void ComlinkWidget::setName(const QString &name)
{
	nameLineEdit->setText(name);
}

void ComlinkWidget::sendMessage(const QStringList &message)
{
	if (m_comlinkSocket.state() == QUdpSocket::BoundState)
		m_comlinkSocket.writeDatagram({message.join(c_separator).toUtf8(), m_connectedAddress, c_port});
}

const QSet<QString> &ComlinkWidget::comlinks() const
{
	return m_knownComlinks;
}

void ComlinkWidget::connectComlink(const QString &comlink)
{
	auto items{listWidget->findItems(comlink, Qt::MatchExactly)};

	if (items.count() == 1)
		connectComlink(items.first());
}

void ComlinkWidget::connectActions()
{
	connect(actionConnect, &QAction::triggered, [this]
	{
		connectComlink(listWidget->currentItem());
	});
	connect(actionDisconnect, &QAction::triggered, [this]
	{
		for (auto *item: listWidget->findItems("*", Qt::MatchWildcard))
			updateIcon(item, palette().window().color());
		m_connectedAddress = QHostAddress{};
		m_comlinkSocket.disconnectFromHost();
	});
}

void ComlinkWidget::connectWidgets()
{
	auto *contextMenu{new QMenu{this}};

	contextMenu->addActions({actionConnect, actionDisconnect});
	connect(listWidget, &QListWidget::customContextMenuRequested, [this, contextMenu](const QPoint &pos)
	{
		auto index{listWidget->indexAt(pos)};

		if (index.isValid())
		{
			listWidget->item(index.row())->setSelected(true);
			contextMenu->exec(listWidget->mapToGlobal(pos));
		}
	});
}

void ComlinkWidget::connectSockets()
{
	m_broadCastSocket.bind(c_port, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
	m_comlinkSocket.bind(QHostAddress{m_addresses.first()}, c_port, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
	connect(&m_broadCastSocket, &QUdpSocket::readyRead, [this]
	{
		while (m_broadCastSocket.hasPendingDatagrams())
		{
			QString datagram{m_broadCastSocket.receiveDatagram().data()};
			auto	data{datagram.split(c_separator)};
			bool	ok;
			int		header{data.at(Header).toInt(&ok)};

			if (header == Identity && !m_knownComlinks.contains(datagram))
			{
				auto *item{new QListWidgetItem};

				item->setText(datagram);
				item->setData(Qt::UserRole, data);
				updateIcon(item, palette().window().color());
				listWidget->addItem(item);
				m_knownComlinks << datagram;
			}
			else if (header == Contact)
			{
				m_timer.stop();
				data.pop_front();
				data.prepend(QString::number(Identity));
				updateIcon(listWidget->findItems(data.join(c_separator), Qt::MatchExactly).first(), palette().window().color());
			}
		}
	});
	connect(&m_comlinkSocket, &QUdpSocket::stateChanged, [this](QUdpSocket::SocketState socketState)
	{
		if (socketState == QUdpSocket::BoundState)
			m_timer.stop();
		else
			m_timer.start(c_delay);
	});
	connect(&m_comlinkSocket, &QUdpSocket::readyRead, this, &ComlinkWidget::processMessage);
	connect(&m_timer, &QTimer::timeout, [this]
	{
		QStringList datagram{{QString::number(Identity), nameLineEdit->text()}};

		datagram << m_addresses;
		m_broadCastSocket.writeDatagram({datagram.join(c_separator).toUtf8(), QHostAddress::Broadcast, c_port});
	});
}

void ComlinkWidget::connectComlink(QListWidgetItem *comlink)
{
	if (comlink)
	{
		QStringList datagram{{QString::number(Contact), nameLineEdit->text()}};

		datagram << m_addresses;
		for (auto *item: listWidget->findItems("*", Qt::MatchWildcard))
			if (item != comlink)
				updateIcon(item, palette().window().color());
		updateIcon(comlink, s_connectedColor);
		m_connectedAddress = QHostAddress{listWidget->currentItem()->data(Qt::UserRole).toStringList().at(FirstAddress)};
		m_comlinkSocket.disconnectFromHost();
		m_comlinkSocket.bind(m_connectedAddress, c_port, QUdpSocket::ShareAddress | QUdpSocket::ReuseAddressHint);
		m_broadCastSocket.writeDatagram({datagram.join(c_separator).toUtf8(), QHostAddress::Broadcast, c_port});
	}
}

void ComlinkWidget::processMessage()
{
	while (m_comlinkSocket.hasPendingDatagrams())
	{
		QString datagram{m_comlinkSocket.receiveDatagram().data()};

		emit messageReceived(datagram.split(c_separator));
	}
}

void ComlinkWidget::updateIcon(QListWidgetItem *item, const QColor &color)
{
	QPixmap	pixmap{8, 8};

	pixmap.fill(color);
	item->setIcon(pixmap);
}
