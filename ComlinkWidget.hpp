//------------------------------------------------------------------------------
//
// ComlinkWidget.hpp created by Yyhrs 2019-08-14
//
//------------------------------------------------------------------------------

#ifndef COMLINKWIDGET_HPP
#define COMLINKWIDGET_HPP

#include <QTimer>
#include <QUdpSocket>

#include "ui_ComlinkWidget.h"

class ComlinkWidget: public QWidget, private Ui::ComlinkWidget
{
	Q_OBJECT

public:
	explicit ComlinkWidget(QWidget *parent = nullptr);
	~ComlinkWidget();

	void				setName(const QString &name);
	void				sendMessage(const QStringList &message);
	const QSet<QString> &comlinks() const;
	void				connectComlink(const QString &comlink);

signals:
	void messageReceived(const QStringList &message);

private:
	enum Header
	{
		Identity,
		Contact
	};
	enum Datagram
	{
		Header			= 0,
		Name			= 1,
		FirstAddress	= 2
	};

	void	connectActions();
	void	connectWidgets();
	void	connectSockets();

	void	connectComlink(QListWidgetItem *comlink);
	void	processMessage();
	void	updateIcon(QListWidgetItem *item, const QColor &color);

	QUdpSocket		m_broadCastSocket;
	QUdpSocket		m_comlinkSocket;
	QTimer			m_timer;
	QStringList		m_addresses;
	QSet<QString>	m_knownComlinks;
	QHostAddress	m_connectedAddress;

	inline static const QColor	s_connectedColor{61, 174, 233};

	static constexpr quint16	c_port{45454};
	static constexpr int		c_delay{2500};
	static constexpr QChar		c_separator{'|'};
};

#endif // COMLINKWIDGET_HPP
