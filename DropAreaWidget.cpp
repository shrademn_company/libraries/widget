//------------------------------------------------------------------------------
//
// DropAreaWidget.cpp created by Yyhrs 2020-01-07
//
//------------------------------------------------------------------------------

#include <QDragEnterEvent>

#include "DropAreaWidget.hpp"

DropAreaWidget::DropAreaWidget(QWidget *parent):
	QFrame{parent}
{
	setupUi(this);
	state(false);
	setAcceptDrops(true);
	titleLabel->setStyleSheet(QStringLiteral("font-weight: bold; font-size: 24px;"));
	connect(browsePushButton, &QPushButton::clicked, this, &DropAreaWidget::browserClicked);
}

void DropAreaWidget::setText(const QString &title, const QString &button, const QString &subline)
{
	titleLabel->setText(title);
	browsePushButton->setText(button);
	sublineLabel->setText(subline);
}

void DropAreaWidget::dragEnterEvent(QDragEnterEvent *event)
{
	state(true);
	event->acceptProposedAction();
}

void DropAreaWidget::dragLeaveEvent(QDragLeaveEvent *event)
{
	state(false);
	event->accept();
}

void DropAreaWidget::dropEvent(QDropEvent *event)
{
	state(false);
	event->acceptProposedAction();
	emit itemDropped(event->mimeData());
}

void DropAreaWidget::state(bool on)
{
	setStyleSheet(QStringLiteral("DropAreaWidget {border-color: palette(link%1);}").arg(on ? "" : "-visited"));
	titleLabel->setForegroundRole(on ? QPalette::Link : QPalette::Text);
}
