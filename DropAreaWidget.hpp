//------------------------------------------------------------------------------
//
// DropAreaWidget.hpp created by Yyhrs 2020-01-07
//
//------------------------------------------------------------------------------

#ifndef DROPAREAWIDGET_HPP
#define DROPAREAWIDGET_HPP

#include <QMimeData>

#include "ui_DropAreaWidget.h"

class DropAreaWidget: public QFrame, private Ui::DropAreaWidget
{
	Q_OBJECT

public:
	explicit DropAreaWidget(QWidget *parent = nullptr);

	void setText(const QString &title, const QString &button, const QString &subline);

signals:
	void	itemDropped(const QMimeData *mimeData = nullptr);
	void	browserClicked();

protected:
	void	dragEnterEvent(QDragEnterEvent *event) override;
	void	dragLeaveEvent(QDragLeaveEvent *event) override;
	void	dropEvent(QDropEvent *event) override;

private:
	void state(bool on);
};

#endif // DROPAREAWIDGET_HPP
