//------------------------------------------------------------------------------
//
// LogWidget.hpp created by Yyhrs 2017
//
//------------------------------------------------------------------------------

#ifndef LOGWIDGET_HPP
#define LOGWIDGET_HPP

#include <QDir>
#include <QMetaEnum>

#include "TableModel.hpp"
#include "TableView.hpp"

class LogWidget: public TableView
{
	Q_OBJECT

public:
	enum LogType
	{
		Information = 0,
		Success,
		Warning,
		Critical
	};
	Q_ENUM(LogType)
	enum Column
	{
		Time = 0,
		Type,
		Log,
		ColumnCount
	};
	Q_ENUM(Column)

	template<typename Value, typename T>
	using Return = typename std::enable_if<QtPrivate::IsQEnumHelper<Value>::Value, T>::type;

	explicit LogWidget(QWidget *parent = nullptr);
	~LogWidget() = default;

	void setColor(QColor const &color, LogType type);

	void addLog(LogType type, QVariant const &log, QVariantList const &description = {});
	void addLogs(LogType type, QVariant const &log, QList<QVariantList> const &descriptions = {});
	void clear();

	void    setAutoFlush(int interval, QDir const &destination = {});
	QString flush(QDir const &destination = {});
	void    save(QFileInfo const &fileInfo);

	template<typename Header>
	Return<Header, void> addColumns(QList<Header> const &headers);

private:
	TableModel            m_logModel;
	QMetaEnum             m_metaEnum;
	QMap<LogType, QColor> m_colors;
	QTimer                m_timer;
};

#include "LogWidget.tpp"

#endif // LOGWIDGET_HPP
