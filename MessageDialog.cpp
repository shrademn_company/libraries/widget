//------------------------------------------------------------------------------
//
// MessageDialog.cpp created by Yyhrs 2019-12-09
//
//------------------------------------------------------------------------------

#include <QIcon>
#include <QMetaEnum>

#include "MessageDialog.hpp"

MessageDialog::MessageDialog(Type type, const QString &message, QWidget *parent):
	QDialog{parent}
{
	setupUi(this);
	setWindowFlag(Qt::FramelessWindowHint);
	label->setText(message);
	icon->setPixmap(QIcon::fromTheme(QString{QMetaEnum::fromType<Type>().valueToKey(type)}.toLower()).pixmap(32, 32));
	exec();
}
