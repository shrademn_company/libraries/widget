//------------------------------------------------------------------------------
//
// MessageDialog.hpp created by Yyhrs 2019-12-09
//
//------------------------------------------------------------------------------

#ifndef MESSAGEDIALOG_HPP
#define MESSAGEDIALOG_HPP

#include "ui_MessageDialog.h"

class MessageDialog: public QDialog, private Ui::MessageDialog
{
	Q_OBJECT

public:
	enum Type
	{
		Information,
		Question,
		Warning,
		Critical
	};
	Q_ENUM(Type)

	explicit MessageDialog(Type type, const QString &message, QWidget *parent = nullptr);
};

#endif // MESSAGEDIALOG_HPP
