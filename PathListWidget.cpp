//------------------------------------------------------------------------------
//
// PathListWidget.cpp created by Yyhrs 2017
//
//------------------------------------------------------------------------------

#include <QAction>
#include <QDesktopServices>
#include <QDir>
#include <QDrag>
#include <QDropEvent>
#include <QFileDialog>
#include <QFileSystemModel>
#include <QMimeData>
#include <QPainter>

#include "PathListWidget.hpp"

PathListWidget::PathListWidget(QWidget *parent): QListWidget{parent}
{
	setIconSize({16, 16});
	m_actions[Add] = new QAction{QIcon::fromTheme("add"), "Add", this};
	m_actions[Remove] = new QAction{QIcon::fromTheme("remove"), "Remove", this};
	m_actions[Remove]->setDisabled(true);
	m_actions[Open] = new QAction{QIcon::fromTheme("document"), "Open", this};
	m_actions[Open]->setDisabled(true);
	addActions(m_actions.values());
	connect(m_actions[Add], &QAction::triggered, this, [this]
	{
		QStringList paths;

		if (m_type == File)
			paths << QFileDialog::getOpenFileName(this, {}, {}, m_filter);
		else if (m_type == Files)
			paths << QFileDialog::getOpenFileNames(this, {}, {}, m_filter);
		else if (m_type == Folder)
			paths << QFileDialog::getExistingDirectory(this, {});
		for (auto const &path: qAsConst(paths))
			addItem(path);
	});
	connect(m_actions[Remove], &QAction::triggered, this, [this]
	{
		auto        indexes{selectionModel()->selectedRows()};
		QList<int>  rows;

		for (auto const &index: indexes)
			rows << index.row();
		std::sort(rows.begin(), rows.end(), std::greater<int>());
		for (auto const &row: qAsConst(rows))
			model()->removeRow(row);
		emit listModified();
	});
	connect(m_actions[Open], &QAction::triggered, this, [this]
	{
		for (auto index: selectionModel()->selectedIndexes())
			QDesktopServices::openUrl(QUrl::fromLocalFile(index.data(Qt::DisplayRole).toString()));
	});
	connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, [this](QItemSelection const & /*selected*/, QItemSelection const & /*deselected*/)
	{
		m_actions[Remove]->setDisabled(selectionModel()->selectedIndexes().isEmpty());
		m_actions[Open]->setDisabled(selectionModel()->selectedIndexes().isEmpty());
	});
}

const QMap<PathListWidget::Action, QAction *> &PathListWidget::actions() const
{
	return m_actions;
}

void PathListWidget::setOpenOption(PathListWidget::PathType type, QString const &filter)
{
	m_type = type;
	m_filter = filter;
}

void PathListWidget::setBehaviour(Behaviour behaviour)
{
	m_behaviour = behaviour;
}

void PathListWidget::addItem(QString const &path)
{
	QFileSystemModel model;
	auto             addPath{[this, &model](QString const &path)
		{
			int row{count()};

			QListWidget::addItem(QDir::toNativeSeparators(path));
			item(row)->setIcon(model.fileIcon(model.index(path)));
			item(row)->setToolTip(QDir::toNativeSeparators(path));
		}};

	if (m_behaviour != AllEntries && QFileInfo{path}.isDir())
	{
		for (auto &file: QDir{path}.entryInfoList(QDir::Files))
			addPath(file.absoluteFilePath());
		if (m_behaviour == RecursiveFiles)
		{
			for (auto &file: QDir(path).entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot))
				addPath(file.absoluteFilePath());
			return ;
		}
	}
	else
		addPath(path);
	emit listModified();
}

QMimeData *PathListWidget::mimeData(QList<QListWidgetItem *> const &items) const
{
	auto        *mimeData{new QMimeData};
	QList<QUrl> urls;

	for (auto const &item: items)
		urls << QUrl::fromLocalFile(item->text());
	mimeData->setUrls(urls);
	return mimeData;
}

void PathListWidget::dragEnterEvent(QDragEnterEvent *event)
{
	if (event->mimeData()->hasUrls())
		event->acceptProposedAction();
}

void PathListWidget::dragMoveEvent(QDragMoveEvent *event)
{
	if (event->mimeData()->hasUrls())
		event->acceptProposedAction();
}

void PathListWidget::dropEvent(QDropEvent *event)
{
	if (event->mimeData()->hasUrls())
	{
		for (auto const &file: event->mimeData()->urls())
			addItem(file.toLocalFile());
		emit fileDropped();
	}
}
