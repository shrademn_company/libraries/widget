//------------------------------------------------------------------------------
//
// PathListWidget.hpp created by Yyhrs 2017
//
//------------------------------------------------------------------------------

#ifndef PATHLISTWIDGET_HPP
#define PATHLISTWIDGET_HPP

#include <QListWidget>

class PathListWidget: public QListWidget
{
	Q_OBJECT

public:
	enum PathType
	{
		File = 0,
		Files,
		Folder,
	};
	enum Behaviour
	{
		AllEntries,
		RootFiles,
		RecursiveFiles
	};
	enum Action
	{
		Add,
		Remove,
		Open
	};
	Q_ENUM(Action)

	explicit PathListWidget(QWidget *parent = nullptr);

	const QMap<PathListWidget::Action, QAction *> &actions() const;
	void                                          setOpenOption(PathType type, QString const &filter = {});
	void                                          setBehaviour(Behaviour behaviour);
	void                                          addItem(QString const &path);

signals:
	void listModified();
	void fileDropped();

protected:
	QMimeData *mimeData(QList<QListWidgetItem *> const &items) const override;
	void      dragEnterEvent(QDragEnterEvent *event) override;
	void      dragMoveEvent(QDragMoveEvent *event) override;
	void      dropEvent(QDropEvent *event) override;

private:
	PathType                m_type{File};
	QString                 m_filter;
	Behaviour               m_behaviour{AllEntries};
	QMap<Action, QAction *> m_actions;
};

#endif // PATHLISTWIDGET_HPP
