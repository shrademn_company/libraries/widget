//------------------------------------------------------------------------------
//
// TableModel.cpp created by Yyhrs 2019-12-30
//
//------------------------------------------------------------------------------

#include <QDataStream>
#include <QMimeData>

#include "TableModel.hpp"

TableModel::TableModel(QObject *parent):
	QAbstractTableModel{parent}
{
}

void TableModel::operator<<(QDataStream &stream)
{
	QList<Row> rows;

	stream >> rows;
	addRows(rows);
}

void TableModel::operator>>(QDataStream &stream) const
{
	stream << m_storage;
}

Qt::DropActions TableModel::supportedDropActions() const
{
	return Qt::CopyAction | Qt::MoveAction;
}

Qt::DropActions TableModel::supportedDragActions() const
{
	return Qt::CopyAction | Qt::MoveAction;
}

QStringList TableModel::mimeTypes() const
{
	return {s_rowListMime};
}

QMimeData *TableModel::mimeData(const QModelIndexList &indexes) const
{
	auto           *mimeData{new QMimeData};
	QByteArray     encoded;
	QDataStream    stream{&encoded, QDataStream::WriteOnly};
	QMap<int, Row> rows;

	for (const auto index: indexes)
		if (!rows.contains(index.row()))
			rows[index.row()] = m_storage[index.row()];
	stream << rows;
	mimeData->setData(s_rowListMime, encoded);
	return mimeData;
}

bool TableModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int /*column*/, const QModelIndex & /*parent*/)
{
	QByteArray     encoded{data->data(s_rowListMime)};
	QDataStream    stream{&encoded, QDataStream::ReadOnly};
	QMap<int, Row> rows;

	stream >> rows;
	if (action == Qt::CopyAction)
	{
		addRows(rows.values());
		return true;
	}
	else if (action == Qt::MoveAction && row > -1)
	{
		int shift{0};

		for (int key: rows.keys())
		{
			beginMoveRows({}, key, key, {}, row);
			m_storage.move(key + shift, (key < row ? row - 1 : row) + shift);
			endMoveRows();
			shift = (key < row ? shift + 1 : shift - 1);
		}
	}
	return false;
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
	if (index.isValid())
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsDragEnabled;
	return Qt::ItemIsDropEnabled;
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role == Qt::DisplayRole)
	{
		if (orientation == Qt::Horizontal && section < m_horizontalHeaders.count())
			return m_horizontalHeaders.value(section);
		else if (orientation == Qt::Vertical && section < m_verticalHeaders.count())
			return m_verticalHeaders.value(section);
	}
	return QAbstractTableModel::headerData(section, orientation, role);
}

int TableModel::columnCount(const QModelIndex & /*parent*/) const
{
	return m_horizontalHeaders.count();
}

QList<int> TableModel::rows() const
{
	QList<int> rows;

	for (int row{0}; row < rowCount(); ++row)
		rows << row;
	return rows;
}

int TableModel::rowCount(const QModelIndex & /*parent*/) const
{
	return m_storage.count();
}

void TableModel::addRows(int count)
{
	beginInsertRows({}, rowCount(), rowCount() + count - 1);
	while (count--)
		m_storage.append(Row{columnCount()});
	endInsertRows();
}

void TableModel::addRows(const QList<Row> &rows)
{
	if (rows.isEmpty())
		return ;
	beginInsertRows({}, rowCount(), rowCount() + rows.count() - 1);
	m_storage << rows;
	endInsertRows();
}

bool TableModel::insertRows(int position, int rows, const QModelIndex &parent)
{
	Row row;

	row.resize(columnCount());
	beginInsertRows(parent, position, position + rows - 1);
	m_storage.insert(position, row);
	endInsertRows();
	return true;
}

void TableModel::insertRows(const QList<QPair<int, Row>> &rows)
{
	int count{1};

	if (rows.isEmpty())
		return ;
	while (count < rows.length() && rows.at(count - 1).first == rows.at(count).first - 1)
		++count;
	beginInsertRows({}, rows.first().first, rows.at(count - 1).first);
	for (const auto &row: rows.mid(0, count))
		m_storage.insert(row.first, row.second);
	endInsertRows();
	if (count < rows.count())
		insertRows(rows.mid(count, rows.count() - count));
}

void TableModel::removeRows(QList<int> rows)
{
	int count{1};

	if (rows.isEmpty())
		return ;
	std::sort(rows.begin(), rows.end(), std::greater<>());
	while (count < rows.length() && rows.at(count - 1) == rows.at(count) + 1)
		++count;
	beginRemoveRows({}, rows.at(count - 1), rows.first());
	for (const auto &row: rows.mid(0, count))
		m_storage.removeAt(row);
	endRemoveRows();
	if (count < rows.count())
		removeRows(rows.mid(count, rows.count() - count));
}

void TableModel::takeRows(const QList<int> &rows, QList<Row> &result)
{
	int count{1};

	if (rows.isEmpty())
		return ;
	while (count < rows.length() && rows.at(count - 1) == rows.at(count) + 1)
		++count;
	beginRemoveRows({}, rows.at(count - 1), rows.first());
	for (const auto &row: rows.mid(0, count))
		result << m_storage.takeAt(row);
	endRemoveRows();
	if (count < rows.count())
		takeRows(rows.mid(count, rows.count() - count), result);
}

void TableModel::clear()
{
	if (m_storage.count())
	{
		beginRemoveRows({}, 0, m_storage.count() - 1);
		m_storage.clear();
		endRemoveRows();
	}
}

QVariant TableModel::data(const QModelIndex &index, int role) const
{
	return data(index.row(), index.column(), role);
}

QVariant TableModel::data(int row, int column, int role) const
{
	return m_storage[row][column][role == Qt::EditRole ? Qt::DisplayRole : role];
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	return setData(index.row(), index.column(), value, role);
}

bool TableModel::setData(int row, int column, const QVariant &value, int role)
{
	if (m_storage[row][column][role == Qt::EditRole ? Qt::DisplayRole : role] != value)
	{
		m_storage[row][column][role == Qt::EditRole ? Qt::DisplayRole : role] = value;
		emit QAbstractTableModel::dataChanged(index(row, column), index(row, column), {role});
	}
	return true;
}

const TableModel::Row &TableModel::rowData(int row) const
{
	return m_storage[row];
}

QMap<int, QVariant> TableModel::itemData(const QModelIndex &index) const
{
	QMap<int, QVariant> result;

	for (auto role: m_storage[index.row()][index.column()].keys())
		result.insert(role, QVariant::fromValue(m_storage[index.row()][index.column()][role]));
	return result;
}

bool TableModel::setItemData(const QModelIndex &index, const QMap<int, QVariant> &roles)
{
	for (auto role: roles.keys())
		m_storage[index.row()][index.column()][role] = roles[role].toMap();
	return true;
}
