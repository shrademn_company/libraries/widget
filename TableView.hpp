//------------------------------------------------------------------------------
//
// TableView.hpp created by Yyhrs 2017
//
//------------------------------------------------------------------------------

#ifndef TABLEVIEW_HPP
#define TABLEVIEW_HPP

#include <QAbstractProxyModel>
#include <QDialog>
#include <QLabel>
#include <QTableView>
#include <QTableWidget>
#include <QTimer>
#include <QVBoxLayout>

class TableView: public QTableView
{
	Q_OBJECT
	Q_PROPERTY(int searchTableHeight READ searchTableHeight WRITE setSearchTableHeight CONSTANT)

public:
	enum Action
	{
		ToggleFilter,
		AdjustColumns,
		AdjustRows,
		HideColumns,
		ShowAllColumns
	};

	explicit TableView(QWidget *parent = nullptr);
	~TableView() override = default;

	void                setModel(QAbstractItemModel *model) override;
	QAbstractItemModel  &sourceModel();
	QAbstractProxyModel &proxyModel();

	const QMap<Action, QAction *> &actions() const;
	QList<QAction *> const        &columnActions() const;

	void            setColumnsHidden(QList<int> const &columns, bool hide);
	void            setColumnHidden(int column, bool hide);
	void            hideColumn(int column);
	void            showColumn(int column);
	void            hideSelectedColumns();
	void            resizeSelectedColumnsToContents();
	QList<int>      logicalRows();
	QList<int>      logicalSelectedRows();
	QList<int>      visualSelectedRows();
	QModelIndexList logicalSelectedIndexes(int column = 0);
	QModelIndexList visualSelectedIndexes(int column = 0);

	virtual void setZoomPercentage(int zoomPercentage);
	void         refreshZoomView();

	void         refreshSearchTable();
	virtual void setFilter(bool on);
	virtual void addFilter(QSet<int> const &rows);
	virtual void addFilter(int column, QString const &text);

signals:
	void zoomChanged(int zoomPourcentage);
	void filterRowsAdded(QSet<int> const &rows);
	void filterPatternAdded(int column, QString const &pattern);
	void columnHidden(int column, bool hide);

protected:
	void wheelEvent(QWheelEvent *event) override;
	void keyPressEvent(QKeyEvent *event) override;

	virtual QAbstractProxyModel *createProxyModel() const;

	virtual void invalidFilters();

	int  searchTableHeight();
	void setSearchTableHeight(int height);
	void setSearchTableHidden(bool hide);

	virtual void copy();

	void connectHeader();

private:
	Q_DISABLE_COPY(TableView)

	void connectActions();
	void connectWidgets();

	void showSizeDialog();

	QAbstractProxyModel     *m_tableProxyModel{nullptr};
	QDialog                 *m_sizeDialog{nullptr};
	QLabel                  *m_sizeLabel{nullptr};
	QTableWidget            m_searchTable;
	QVBoxLayout             m_searchTableLayout;
	QMap<Action, QAction *> m_actions;
	QList<QAction *>        m_columnActions;
	int                     m_height{0};
	int                     m_currentSection{0};
	QTimer                  m_zoomTimer;
	int                     m_zoomPourcentage{100};
	int                     m_zoomBaseSize{0};

	inline static QString const s_sizeFormat{QStringLiteral("<font size='36' color='#4d4d4d' face='consolas'>%1%</font>")};
};

#endif // TABLEVIEW_HPP
