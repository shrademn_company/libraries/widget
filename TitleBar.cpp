//------------------------------------------------------------------------------
//
// TitleBar.cpp created by Yyhrs 2021-04-30
//
//------------------------------------------------------------------------------

#include <QApplication>
#include <QMoveEvent>
#include <QScreen>
#include <QWindow>

#include "TitleBar.hpp"

TitleBar::TitleBar(QWidget *parent):
	QWidget{parent}
{
	setupUi(this);
	titleLabel->setText(QApplication::applicationName());
	iconLabel->setPixmap(topLevelWidget()->windowIcon().pixmap({16, 16}));
	topLevelWidget()->setWindowFlag(Qt::FramelessWindowHint);
	topLevelWidget()->installEventFilter(this);
	addAction(actionLock);
	connect(actionLock, &QAction::toggled, this, [this](bool checked)
	{
		leftToolButton->setHidden(checked);
		rightToolButton->setHidden(checked);
		topToolButton->setHidden(checked);
	});
	connect(leftToolButton, &QToolButton::clicked, this, [this]
	{
		auto available{screen()->availableGeometry()};

		topLevelWidget()->move(available.topLeft());
		topLevelWidget()->resize(available.width() / 2, available.height());
	});
	connect(rightToolButton, &QToolButton::clicked, this, [this]
	{
		auto available{screen()->availableGeometry()};

		topLevelWidget()->move(available.topLeft().x() + available.width() / 2, available.topLeft().y());
		topLevelWidget()->resize(available.width() / 2, available.height());
	});
	connect(topToolButton, &QToolButton::clicked, this, [this]
	{
		auto available{screen()->availableGeometry()};
		auto geometry{topLevelWidget()->geometry()};

		geometry.setY(0);
		geometry.setHeight(available.height());
		topLevelWidget()->setGeometry(geometry);
	});
	connect(closeToolButton, &QToolButton::clicked, topLevelWidget(), [this]
	{
		if (qApp->quitOnLastWindowClosed())
			QApplication::quit();
		else
			topLevelWidget()->close();
	});
}

TitleBar::Flags TitleBar::flags() const
{
	Flags flags;
	auto  available{screen()->availableGeometry()};
	auto  width{available.width()};
	auto  geometry{topLevelWidget()->geometry()};

	available.setWidth(width / 2);
	flags.setFlag(Lock, actionLock->isChecked());
	flags.setFlag(Left, available == geometry);
	available.setWidth(width);
	available.setX(available.topLeft().x() + width / 2);
	flags.setFlag(Right, available == geometry);
	flags.setFlag(Top, available.height() == geometry.height());
	return flags;
}

void TitleBar::setFlags(Flags flags)
{
	actionLock->setChecked(flags.testFlag(Lock));
	if (flags.testFlag(Left))
		leftToolButton->click();
	else if (flags.testFlag(Right))
		rightToolButton->click();
	else if (flags.testFlag(Top))
		topToolButton->click();
}

void TitleBar::insertWidget(int page, QWidget *widget)
{
	stackedWidget->insertWidget(page, widget);
}

void TitleBar::setCurrentIndex(int page)
{
	stackedWidget->setCurrentIndex(page);
}

bool TitleBar::eventFilter(QObject *watched, QEvent *event)
{
	auto *window{qobject_cast<QWindow *>(watched)};

	if (!m_installed && watched == topLevelWidget() && event->type() == QEvent::Show)
	{
		auto const windows{qApp->topLevelWindows()};

		for (auto *window: windows)
			window->installEventFilter(this);
		m_installed = true;
	}
	else if (!actionLock->isChecked() && window)
	{
		if (event->type() == QEvent::MouseMove && window->windowState() == Qt::WindowNoState)
		{
			auto position{dynamic_cast<QMouseEvent *>(event)->globalPosition()};

			m_edges = {};
			m_edges.setFlag(Qt::LeftEdge, qAbs(position.x() - window->geometry().x()) < 6);
			m_edges.setFlag(Qt::RightEdge, qAbs(position.x() - window->geometry().x() - window->geometry().width()) < 6);
			m_edges.setFlag(Qt::TopEdge, qAbs(position.y() - window->geometry().y()) < 6);
			m_edges.setFlag(Qt::BottomEdge, qAbs(position.y() - window->geometry().y() - window->geometry().height()) < 6);
			if (m_edges == (Qt::LeftEdge | Qt::TopEdge) || m_edges == (Qt::RightEdge | Qt::BottomEdge))
				window->setCursor(Qt::SizeFDiagCursor);
			else if (m_edges == (Qt::LeftEdge | Qt::BottomEdge) || m_edges == (Qt::RightEdge | Qt::TopEdge))
				window->setCursor(Qt::SizeBDiagCursor);
			else if (m_edges == Qt::LeftEdge || m_edges == Qt::RightEdge)
				window->setCursor(Qt::SizeHorCursor);
			else if (m_edges == Qt::TopEdge || m_edges == Qt::BottomEdge)
				window->setCursor(Qt::SizeVerCursor);
			else if (window->cursor() != Qt::ArrowCursor)
				window->setCursor(Qt::ArrowCursor);
		}
		else if (event->type() == QEvent::MouseButtonPress && m_edges != Qt::Edges{})
		{
			window->startSystemResize(m_edges);
			return true;
		}
	}
	return false;
}

void TitleBar::mouseMoveEvent(QMouseEvent *event)
{
	if (!actionLock->isChecked() && geometry().contains(event->pos()))
	{
		topLevelWidget()->windowHandle()->startSystemMove();
		event->accept();
	}
}

void TitleBar::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (geometry().contains(event->pos()))
	{
		if (topLevelWidget()->windowState() == Qt::WindowNoState)
		{
			topLevelWidget()->showMaximized();
			topLevelWidget()->resize(screen()->availableGeometry().size());
		}
		else
			topLevelWidget()->showNormal();
	}
}
