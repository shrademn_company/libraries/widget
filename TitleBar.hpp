//------------------------------------------------------------------------------
//
// TitleBar.hpp created by Yyhrs 2021-04-30
//
//------------------------------------------------------------------------------

#ifndef TITLEBAR_HPP
#define TITLEBAR_HPP

#include <QHBoxLayout>
#include <QLabel>
#include <QToolButton>

#include "ui_TitleBar.h"

class TitleBar: public QWidget, private Ui::TitleBar
{
	Q_OBJECT

public:
	enum Flag
	{
		Lock  = 0x01,
		Left  = 0x02,
		Right = 0x04,
		Top   = 0x08
	};
	Q_DECLARE_FLAGS(Flags, Flag)

	explicit TitleBar(QWidget *parent = nullptr);

	Flags flags() const;
	void  setFlags(Flags flags);
	void  insertWidget(int page, QWidget *widget);
	void  setCurrentIndex(int page);

	bool eventFilter(QObject *watched, QEvent *event) override;

protected:
	void mouseMoveEvent(QMouseEvent *event) override;
	void mouseDoubleClickEvent(QMouseEvent *event) override;

private:
	bool      m_installed{false};
	Qt::Edges m_edges;
};

#endif // TITLEBAR_HPP
