#-------------------------------------------------
#
# WidgetComlink.pri created by Shrademn
#
#-------------------------------------------------

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

QT += network

HEADERS += \
    $$PWD/ComlinkWidget.hpp

SOURCES += \
    $$PWD/ComlinkWidget.cpp

FORMS += \
    $$PWD/ComlinkWidget.ui
