#-------------------------------------------------
#
# WidgetEnhanced.pri created by Shrademn
#
#-------------------------------------------------

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
    $$PWD/ColorButton.cpp \
    $$PWD/MessageDialog.cpp \
    $$PWD/TableModel.cpp \
    $$PWD/TableView.cpp \
    $$PWD/TableProxyModel.cpp \
    $$PWD/PathLineEdit.cpp \
    $$PWD/PathListWidget.cpp \
    $$PWD/ProgressBarButton.cpp \
    $$PWD/DockWidget.cpp \
    $$PWD/TabWidget.cpp

HEADERS += \
	$$PWD/ColorButton.hpp \
	$$PWD/MessageDialog.hpp \
	$$PWD/TableModel.hpp \
	$$PWD/TableModel.tpp \
	$$PWD/TableView.hpp \
	$$PWD/TableProxyModel.hpp \
	$$PWD/PathLineEdit.hpp \
	$$PWD/PathListWidget.hpp \
	$$PWD/ProgressBarButton.hpp \
	$$PWD/DockWidget.hpp \
	$$PWD/TabWidget.hpp

FORMS += \
	$$PWD/MessageDialog.ui

