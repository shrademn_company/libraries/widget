#-------------------------------------------------
#
# WidgetMeta.pri created by Shrademn
#
#-------------------------------------------------

INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
	$$PWD/LogWidget.cpp \
	$$PWD/DropAreaWidget.cpp \
	$$PWD/CommandWidget.cpp \
	$$PWD/BannerWidget.cpp \
	$$PWD/TitleBar.cpp

HEADERS += \
	$$PWD/DropAreaWidget.hpp \
    $$PWD/LogWidget.hpp \
    $$PWD/CommandWidget.hpp \
    $$PWD/BannerWidget.hpp \
	$$PWD/LogWidget.tpp \
	$$PWD/TitleBar.hpp

FORMS += \
	$$PWD/DropAreaWidget.ui \
	$$PWD/TitleBar.ui
